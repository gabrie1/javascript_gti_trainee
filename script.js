let json;

let httpRequest;

if(window.XMLHttpRequest)
{
    httpRequest = new XMLHttpRequest();
}
else if(window.ActiveXObject)
{
    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
}

httpRequest.open('GET', 'https://quiz-trainee.herokuapp.com/questions', true);
httpRequest.send();

httpRequest.onreadystatechange = function()
{
    if(this.readyState === 4)
    {
        if(this.status === 200)
        {
            json = JSON.parse(this.responseText);
        }
        else
        {
            alert('Houve um erro com a requisição ;=; ');
        }
    }
}

let i = -1;
cont = 0;//Servirá para sabermos se o quiz está sendo feito pela primeira vez ou não
let a;// Essa variável servirá apenas para transformar a variável i em String
let j;
let b;// Essa variável servirá apenas para transformar a variável j em String
let resultado = 0;//Calculará o resultado
let vetor = [];//Esse vetor armazenará os índices relativos aos valores do json que serão adiconados ao resultado

function mostrarQuestao()
{
    if( i != -1)//Não faremos essa operação com o botão COMEÇAR(não há opções a serem marcadas)
    {
        for(j=0; j< json[a]['options'].length; j++)//Evita que o botão possa ser pressionado sem haver uma opção marcada
        {
            if(document.getElementsByName("resposta")[j].checked == true)
                break;
            
            else if(j == json[a]['options'].length - 1)
                return;
        }
    }
    else
    {
        i++;// caso i seja -1, queremos atualizar seu valor para 0, para ele se tornar um índice de json válido
    }
    if(i < json.length)
    {
        a = i.toString();// a é apenas i transformado no tipo String

        document.getElementById("resultado").style.display = "none";//Oculta o resultado
        document.getElementById("listaRespostas").style.display = "inline" ;//torna a lista visível
        document.getElementById("titulo").innerHTML = json[a].title;//atualiza o título com base no json
        document.getElementById("confirmar").innerHTML = "PRÓXIMA";//muda o nome no botão

        for(j=0; j<json[a]['options'].length; j++)
        {
            b = j.toString();// b é apenas j transformado no tipo String
            document.getElementsByTagName("span")[j].innerHTML = json[a]['options'][b]['answer'];//atualiza as respostas
            document.getElementsByName("resposta")[j]['value'] = json[a]['options'][j]['value'];//atualiza o valor de cada resposta
        }

        for(j=0; j< json[a]['options'].length; j++)
        {

            if(document.getElementsByName("resposta")[j].checked == true)
            {
                console.log(i);//Apenas para me auxiliar na visualização do funcionamento do código
                console.log(vetor);//Apenas para me auxiliar na visualização do funcionamento do código
                vetor.push(j);//O vetor recebe o valor do índice onde se encontra, no json, o valor da resposta escolhida
                document.getElementsByName("resposta")[j].checked = false;//Faz a marcação desaparecer na próxima pergunta
            }
        }

        i++//Atualiza o i
    }

    /*O código acima apresenta um problema: quando o botão "COMEÇAR" é pressionado não há nenhuma alternativa a ser
    marcada, mesmo assim o i é atualizado, pois a função mostrarQuestão() é chamada. Dessa forma, o valor de i começará
    em 1 e irá até 4 (json.length). Dessa forma nosso vetor armazenará 4 (json.length - 1) valores ao invés de 
    5 (json.lenght) valores. Precisamos, portanto, fazer mais uma interação para obter esse úlyimo valor não resgatado*/
    else if(i == json.length)
    {
        i--;//decrementamos o i, pois o valor json.length não é conveniente(fora dos limites)
        a = i.toString();

        for(j=0; j< json[a]['options'].length; j++)
        {

            if(document.getElementsByName("resposta")[j].checked == true)//fazemos novamente a última interação
            {
                console.log(i);
                console.log(vetor);
                vetor.push(j);//Pegamos o índice do último valor e armazenamos no vetor
            }
        }

        /*Há ainda outro problema. Quando o botão "Refazer quiz" é pressionado, o valor de i é novamente atualizado.
        No entanto, quando essa atualização ocorre não há alternativas a serem marcadas, nem valores a serem recebidos.
        Desse modo o vetor receberá nessa interação um valor sem significada para nós. Resolvemos isso verificando se 
        o quiz está sendo feito pela primeira vez ou não através da variável cont*/

        if(cont > 0)//Se a variável cont for maior que zero, o quiz está sendo refeito e vetor recebe um valor inútil
        {
            vetor.splice(0, 1);//Aqui removemos o primeiro elemento do vetor. Ele não possui significado para nós
        }

        finalizarQuiz();//Finalizamos o quiz
        return;
    }
}

function finalizarQuiz()
{
    for(i=0; i < vetor.length; i++)//Percorremos o vetor
    {
        let a;
        a = i.toString();
        //Vemos no json os valores relativos aos índices do vetor e adicionamos esses valores à variável resultado
        resultado = resultado + json[a]['options'][vetor[i]]['value'];
    }
    vetor.splice(0, vetor.length);//Resetamos o vetor
    resultado = (resultado/(json.length*3))*100;//Calculamos a porcentagem
    document.getElementById("confirmar").innerHTML = 'Refazer quiz';//Alteramos o nome do botão
    document.getElementById("listaRespostas").style.display = "none";//Ocultamos a lista
    document.getElementById("resultado").style.display = "block";//Toena o resultado visível
    document.getElementById("titulo").innerHTML = 'QUIZ DOS VALORES DA GTI';//Altera o título
    document.getElementById("resultado").innerHTML = 'Sua pontuação foi: ' + resultado + '%';//"Printa" o resultado
    i = -1;//Atualiza i para o valor inicial
    resultado = 0;//Atualiza resultado para o valor inicial 
    cont++;//cont é incrementado, pois precisamos marcar se esta é ou não a primeira vez que o quiz é feito
    console.log(vetor);//Apenas para me auxiliar 
}
